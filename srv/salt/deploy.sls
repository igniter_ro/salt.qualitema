# 1. Install Requirements (development environment)

install_dev_environment:
  pkg.installed:
    - pkgs:
      - git
      - nodejs-legacy
      - openjdk-7-jdk
      - openjdk-7-jre-headless

# 2. Deploy "Hello World" apps

add_deploy_user:
  user.present:
    - name: deploy
    - home: /home/deploy

ensure_ssh_dir:
  file.directory:
    - name: /home/deploy/.ssh
    - user: deploy
    - group: deploy
    - mode: 700
    - makedirs: True

manage_my_ssh_key:
  file.managed:
    - name: /home/deploy/.ssh/{{ pillar['my_ssh_key_name'] }}
    - mode: 600
    - user: deploy
    - group: deploy
    - contents_pillar: my_ssh_key

bitbucket.org:
  ssh_known_hosts:
    - present
    - user: deploy
    - fingerprint: 97:8c:1b:f2:6f:14:6b:5c:3b:ec:aa:46:46:74:7c:40

ensure_deploy_dir:
  file.directory:
    - name: /app
    - user: deploy
    - group: deploy
    - mode: 755
    - makedirs: True

clone_apps:
  cmd:
    - run
    - unless: test -d /app/qualitema
    - user: deploy
    - name: >
              git clone git@bitbucket.org:igniter_ro/qualitema.git /app/qualitema
    - require:
      - pkg: install_dev_environment 
      - ssh_known_hosts: bitbucket.org

build_java:
  cmd:
    - run
    - cwd: /app/qualitema/java
    - unless: test -f /app/qualitema/java/build/libs/gs-spring-boot-0.1.0.jar
    - user: deploy
    - name: >
               export JAVA_HOME=$(java_dir=`readlink /etc/alternatives/java` && echo ${java_dir%/jre/bin/java}) && ./gradlew build
    - require:
      - pkg: install_dev_environment 

# 3. Install and configure services (nginx & supervisor)

install_services:
  pkg.installed:
    - pkgs:
      - nginx
      - supervisor

/etc/nginx/nginx.conf:
  file.managed:
    - source: salt://services/nginx/nginx.conf.jin
    - template: jinja

/etc/nginx/conf.d/nginx_json_logging.conf:
  file.managed:
    - source: salt://services/nginx/nginx_json_logging.conf

/etc/nginx/sites-enabled/qualiapps.conf:
  file.managed:
    - source: salt://services/nginx/qualiapps.conf.jin
    - template: jinja

/etc/supervisor/conf.d/qualitema.conf:
  file.managed:
    - source: salt://services/supervisor/supervisor.conf.jin
    - template: jinja

# 4. Install EFK Stack (Elasticsearch, FluentD, Kibana)

install_elasticsearch_repo:
  pkgrepo.managed:
    - humanname: ElasticSearch Repo
    - name: deb http://packages.elastic.co/elasticsearch/2.x/debian stable main
    - dist: stable
    - file: /etc/apt/sources.list.d/elasticsearch.list
    - keyid: D88E42B4
    - keyserver: pgp.mit.edu

install_elasticsearch:
  pkg.installed:
    - name: elasticsearch

/etc/elasticsearch/elasticsearch.yml:
  file.managed:
    - source: salt://services/efk/elasticsearch.yml

install_fluentd:
  cmd:
    - run
    - unless: test -f /usr/sbin/td-agent
    - name: >
               curl -L https://toolbelt.treasuredata.com/sh/install-debian-wheezy-td-agent2.sh | sh

install_fluentd_elasticsearch_plugin:
  cmd:
    - run
    - unless: td-agent-gem list 2> /dev/null | grep fluent-plugin-elasticsearch2
    - name: >
               td-agent-gem install fluent-plugin-elasticsearch

/etc/td-agent/td-agent.conf:
  file.managed:
    - source: salt://services/efk/td-agent.conf

install_kibana:
  cmd:
    - run
    - unless: test -d /opt/kibana
    - name: >
               mkdir -p /opt/kibana && wget -qO- https://download.elastic.co/kibana/kibana/kibana-4.2.0-linux-x64.tar.gz | tar xvz --strip=1 -C /opt/kibana

add_kibana_user:
  user.present:
    - name: kibana
    - home: /opt/kibana

install_kibana_init_script:
  file.managed:
    - name: /etc/init.d/kibana
    - source: salt://services/efk/kibana-4.x-init
    - mode: 755

kibana_permissions:
  file.directory:
    - name: /opt/kibana
    - user: kibana
    - group: kibana
    - mode: 755
    - recurse:
      - user
      - group

install_kibana_nginx_conf:
  file.managed:
    - name: /etc/nginx/sites-enabled/kibana.conf
    - source: salt://services/nginx/kibana.conf

upstart_nginx:
  service.running:
    - name: nginx
    - enable: True

upstart_kibana:
  service.running:
    - name: kibana
    - enable: True

upstart_fluentd:
  service.running:
    - name: td-agent
    - enable: True

upstart_supervisor:
  service.running:
    - name: supervisor
    - enable: True

upstart_elasticsearch:
  service.elasticsearch:
    - name: elasticsearch
    - enable: True
