# Install and configure a salt-master node
## Install salt-master node with salt-cloud
```
wget -O install_salt.sh https://bootstrap.saltstack.com
sh install_salt.sh -M
apt-get install salt-cloud
```
## Install deploy.sls state file
This will be used to provision our "apps-nginx-efk-1" box.
```
mkdir -p /srv/{salt,pillar}
cp -R ./srv/* /srv
```

## 
# Install and configure salt-cloud
* create a new provider config - /etc/salt/cloud.providers.d/do.conf
```
do:
  provider: digital_ocean
  personal_access_token: [...] 
  ssh_key_file: [...]
  ssh_key_names: [...]
  location: Frankfurt 1
```
* create a new profile for DO instances - /etc/salt/cloud.profiles.d/do.conf
```
base-env:
  provider: do
  image: debian-7-0-x64
  size: 1gb
  location: fra1
  private_networking: True

qualitema:
  extends: base-env
  minion:
    grains:
      roles: aio-server
      env: test
```
* create a new mapping for our new instances - /etc/salt/cloud.maps.d/deploy-app-nginx.map
```
qualitema:
  - apps-nginx-efk-1
```

# Deploy the 'apps-nginx-efk-1' instance
```
salt-cloud -m /etc/salt/cloud.maps.d/deploy-app-nginx.map
```

# Provision the 'apps-nginx-efk-1' instance
```
salt -G 'roles:aio-server' state.sls deploy
```
## What exactly "deploy.sls" does?
* installs dev tools & environment - git, nodejs, openjdk-7-{jdk,jre}
* adds the "deploy" user along with the private key (the public key is used as deploy key for bitbucket.org:igniter_ro/qualitema.git repo)
* creates "/app" directory - here we will clone the qualitema.git repo (it contains provided apps - nodejs/python/java)
* builds the java app - export JAVA_HOME=$(java_dir=`readlink /etc/alternatives/java` && echo ${java_dir%/jre/bin/java}) && ./gradlew build
* installs nginx & supervisord
* copies nginx.* (/srv/salt/nginx/*) and supervisor (/srv/salt/supervisor/*) configs
* installs the EFK stack (Elasticsearch, Fluentd, Kibana)
* enabling and starting services
